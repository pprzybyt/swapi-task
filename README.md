# swapi-task

[![pipeline status](https://gitlab.com/pprzybyt/swapi-task/badges/main/pipeline.svg)](https://gitlab.com/pprzybyt/swapi-task/-/commits/main)
[![coverage report](https://gitlab.com/pprzybyt/swapi-task/badges/main/coverage.svg)](https://gitlab.com/pprzybyt/swapi-task/-/commits/main)

## About the project:

Build a simple app which allows you to collect, resolve and inspect information about characters in the Star Wars
universe from the SWAPI.

The entry endpoint for data retrieval is: https://swapi.co/api/people/

Main parts:

- Collections view
- Fetching newest collection

![](img/main.png)

- Collection detail (+ load more option)

![](img/detail.png)

- Counting values appearances

![](img/value_count.png)

## Requirements:

docker-compose (tested on docker-compose version 1.28.5, build c4eb3a1f)

## How to start:

- You need set up environment variables through `.env` file
- For simplicity just copy `.env.example.local` to `.env`:

> cp .env.example.local .env

- After that You can run

> docker-compose -f local.yml up --build

The app will be reachable at 0.0.0.0:8000

You can also log into admin page (0.0.0.0:8000/admin) using credentials from `.env` file (`user`/`password`)

All important classes can be found in [swapi_collections](swapi_collections)

## How to use:

- fetch:
  - click on `Fetch` button and wait for data
  - when download is finished, appropriate link shlould be added to collection list
- detail:
  - after clicking on desired collection, you will be redirected to its detail
  - you can check the data and load more records (auto-scroll is applied to see newly added rows)
- value count:
  - in collection detail view You can choose the `Value Count` button and explore that functionality
- going back
  - if You want to go back to main page, You can always click on `Collections` from app header

- tests:

> docker-compose -f local.yml exec web python manage.py test

## Potential improvements:

- add celery to handle data downloading (and record the progress)
- refactor templates (move some parts to static files)
- add logs
- add docstrings
- extend tests
