import abc
from typing import List, Dict

import requests
from dateutil import parser
from django.conf import settings

from .file_manager import CsvManager
from .models import Collection


class SwapiClient(metaclass=abc.ABCMeta):
    file_manager = CsvManager()

    def __init__(self, endpoint: str):
        self.url = settings.SWAPI_URL + endpoint

    def _fetch(self) -> List[Dict]:
        response = requests.get(self.url).json()
        data = response['results']
        while response['next']:
            response = requests.get(response['next']).json()
            data.extend(response['results'])
        return data

    @abc.abstractmethod
    def _transform(self, data: List[Dict]) -> List[Dict]:
        pass

    def fetch(self) -> List[Dict]:
        data = self._fetch()
        return self._transform(data)

    def fetch_and_save(self):
        data = self.fetch()
        file_name = self.file_manager.save(data)
        Collection.objects.create(file_name=file_name)


class PeopleClient(SwapiClient):

    def __init__(self):
        super().__init__('people')

    def _transform(self, data: List[Dict]) -> List[Dict]:
        homeworld_data = {}
        keys_to_remove = ['films', 'species', 'vehicles', 'starships', 'created', 'url']

        def fetch_homeworld(homeworld_url):
            homeworld = homeworld_data.get(homeworld_url)
            if homeworld:
                return homeworld
            homeworld = requests.get(homeworld_url).json().get('name')
            homeworld_data[homeworld_url] = homeworld
            return homeworld

        for element in data:
            element['date'] = str(parser.parse(element.pop('edited')).date())
            element['homeworld'] = fetch_homeworld(element['homeworld'])
            for key in keys_to_remove:
                element.pop(key)

        return data
