import abc
import pathlib
import uuid
from typing import List, Dict, Tuple

import petl as etl
from django.conf import settings


def check_if_all_records_returned(func):
    def inner(table, *args, **kwargs):
        result = func(table, *args, **kwargs)
        all_records_returned = len(table) == len(result)
        return result, all_records_returned

    return inner


def confirm_table_is_valid(func):
    def inner(*args, **kwargs):
        result = func(*args, **kwargs)
        try:
            result.len()
        except FileNotFoundError as exc:
            raise FileManagerException(f'Unable to find file: {exc.filename}')
        except etl.errors.FieldSelectionError as exc:
            raise FileManagerException(f'Invalid column name selected: {exc.args}')
        return result

    return inner


class FileManagerException(Exception):
    pass


class FileManager(metaclass=abc.ABCMeta):

    @staticmethod
    def _generate_file_name() -> str:
        return str(uuid.uuid4())

    def _generate_file_path(self, suffix: str) -> str:
        if not suffix:
            raise FileManagerException('No suffix specified')
        if not suffix.startswith('.'):
            suffix = '.' + suffix
        return str(pathlib.PurePath(settings.DOWNLOADS_PATH, self._generate_file_name() + suffix))

    @abc.abstractmethod
    def save(self, data) -> str:
        pass

    @abc.abstractmethod
    def load(self, file_path):
        pass

    @confirm_table_is_valid
    def count_values(self, file_path: str, columns: List[str]):
        table = self.load(file_path)
        value_count = etl.util.counting.valuecounts(table, *columns)

        return value_count

    def get_column_names(self, file_path: str):
        table = self.load(file_path)
        return list(table.columns().keys())

    @staticmethod
    @check_if_all_records_returned
    def _get_top_rows(table: etl.util.base.Table, count: int) -> List[Dict]:
        return list(table[:count + 1])


class CsvManager(FileManager):

    def save(self, data: List[Dict]) -> str:
        table = etl.fromdicts(data)
        file_path = self._generate_file_path('csv')
        etl.tocsv(table, file_path)
        return file_path

    @staticmethod
    @confirm_table_is_valid
    def load(file_path):
        table = etl.fromcsv(file_path)
        return table

    def load_records(self, file_path: str, count: int) -> Tuple[List[Dict], bool]:
        table = self.load(file_path)
        return self._get_top_rows(table, count)

    def load_value_count(self, file_path: str, columns: List[str], count: int) -> Tuple[List[Dict], bool]:
        table = self.count_values(file_path, columns)
        return self._get_top_rows(table, count)
