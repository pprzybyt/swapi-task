from django.contrib import admin

from .models import Collection


class SwapiCollectionsAdmin(admin.ModelAdmin):
    pass


admin.site.register(Collection, SwapiCollectionsAdmin)
