from django.conf import settings
from django.core.exceptions import BadRequest
from django.shortcuts import get_object_or_404, render

from .clients import PeopleClient
from .file_manager import CsvManager, FileManagerException
from .models import Collection


def collection_list(request):
    collections = Collection.objects.all().order_by('-date')
    context = {
        'collections': collections,
    }

    return render(request, 'swapi_collections/base.html', context=context)


def collection_detail(request, collection_id):
    collection = get_object_or_404(Collection, id=collection_id)
    count = request.GET.get('count', settings.LOAD_MORE_INCREMENT)
    count = max(int(count), settings.LOAD_MORE_INCREMENT)

    try:
        content, all_records_returned = CsvManager().load_records(collection.file_name, count=count)
    except FileManagerException as exc:
        raise BadRequest(exc)

    titles, content = content[0], content[1:]
    context = {
        'collection_name': collection.file_name.split('/')[-1],
        'titles': titles,
        'content': content,
        'collection_id': collection_id,
        'all_records_returned': all_records_returned,
        'load_increment': settings.LOAD_MORE_INCREMENT,
    }

    return render(request, 'swapi_collections/collection_detail.html', context=context)


def value_count(request, collection_id):
    collection = get_object_or_404(Collection, id=collection_id)
    selected_columns = request.GET.get('selected_columns', []) or []
    if selected_columns:
        selected_columns = selected_columns.split(',')

    count = request.GET.get('count', settings.LOAD_MORE_INCREMENT)
    count = max(int(count), settings.LOAD_MORE_INCREMENT)

    file_manager = CsvManager()
    titles, content, all_records_returned = [], [], True

    try:
        columns = file_manager.get_column_names(collection.file_name)
        if selected_columns:
            content, all_records_returned = file_manager.load_value_count(
                collection.file_name,
                columns=selected_columns,
                count=count
            )
            titles, content = content[0], content[1:]
    except FileManagerException as exc:
        raise BadRequest(exc)

    context = {
        'collection_name': collection.file_name.split('/')[-1],
        'titles': titles,
        'content': content,
        'collection_id': collection_id,
        'all_records_returned': all_records_returned,
        'load_increment': settings.LOAD_MORE_INCREMENT,
        'columns': columns,
        'selected_columns': selected_columns,
    }
    return render(request, 'swapi_collections/value_count.html', context=context)


def fetch_data(request):
    # TODO add celery to handle loading
    client = PeopleClient()
    client.fetch_and_save()
    return collection_list(request)
