from django.urls import path

import swapi_collections.views as views

urlpatterns = [
    path('', views.collection_list),
    path('fetch-data', views.fetch_data, name='fetch_data'),
    path('collection/<int:collection_id>', views.collection_detail, name='collection_detail'),
    path('collection/<int:collection_id>/value-count', views.value_count, name='value_count'),

]
