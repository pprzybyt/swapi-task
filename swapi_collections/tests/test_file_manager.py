from unittest.mock import patch

import petl
from django.conf import settings
from django.test import TestCase
from parameterized import parameterized

from swapi_collections.file_manager import CsvManager, FileManagerException, check_if_all_records_returned


class FileManagerTestCase(TestCase):

    def setUp(self):
        self.fm = CsvManager()

    @parameterized.expand([
        ('a',),
        ('.a',),
    ])
    def test_generate_file_path(self, suffix: str):
        file_path = self.fm._generate_file_path(suffix=suffix)
        self.assertTrue(file_path.startswith(settings.DOWNLOADS_PATH))
        self.assertTrue(file_path.endswith('.' + suffix.replace('.', '')))

    def test_generate_file_path_with_epty_suffix(self):
        self.assertRaises(FileManagerException, self.fm._generate_file_path, '')

    @patch('petl.fromcsv')
    def test_get_column_names(self, from_csv_mock):
        data = [{'a': 1, 'b': 2}]
        table = petl.fromdicts(data)
        from_csv_mock.return_value = table
        self.assertEqual(self.fm.get_column_names('path'), ['a', 'b'])

        table = petl.fromdicts([])
        from_csv_mock.return_value = table
        self.assertEqual(self.fm.get_column_names('path'), [])

    def test_get_top_rows(self):
        data = [{'a': 1, 'b': 2}]
        table = petl.fromdicts(data)
        self.assertEqual(self.fm._get_top_rows(table, 0), ([('a', 'b')], False))
        self.assertEqual(self.fm._get_top_rows(table, 1), ([('a', 'b'), (1, 2)], True))

    def test_check_if_all_records_returned(self):
        def fun_all(tab):
            return tab

        def fun_not_all(tab):
            return tab + [1]

        table = [1, 2, 3]
        self.assertEqual(check_if_all_records_returned(fun_all)(table), (table, True))
        self.assertEqual(check_if_all_records_returned(fun_not_all)(table), (fun_not_all(table), False))
