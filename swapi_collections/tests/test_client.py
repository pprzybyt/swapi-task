import json
from pathlib import Path
from unittest.mock import patch

from django.test import TestCase

from swapi_collections.clients import PeopleClient


class SwapiClientTestCase(TestCase):
    def setUp(self):
        self.client = PeopleClient()

    def test_fetch(self):
        n = 5

        class HasJsonMethod:
            def __init__(self, data):
                self.data = data

            def json(self):
                return self.data

        def iterator():
            data = [{'results': [i], 'next': 'anything'} for i in range(n)] + [{'results': [n], 'next': None}]
            for response in data:
                yield HasJsonMethod(response)

        it = iterator()

        with patch('requests.get', lambda _: next(it)):
            self.assertEqual(self.client._fetch(), list(range(n + 1)))

    def test_transform(self):
        with open(Path(__file__).parent / 'mock/input.json', 'rb') as f:
            input_object = json.load(f)
        with open(Path(__file__).parent / 'mock/output.json', 'rb') as f:
            output_object = json.load(f)

        self.assertEqual(self.client._transform([input_object]), [output_object])
