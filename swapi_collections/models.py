from django.conf import settings
from django.db import models


class Collection(models.Model):
    file_name = models.FilePathField(path=settings.DOWNLOADS_PATH, unique=True)
    date = models.DateTimeField(auto_now=True)
