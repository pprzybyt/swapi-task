#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset


python /app/manage.py collectstatic --noinput
python /app/manage.py migrate
python /app/manage.py initadmin


/usr/local/bin/gunicorn config.wsgi -w 1 -t 300 -b 0.0.0.0:8000 --chdir=/app --log-file=-
