import os

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.db.utils import IntegrityError

User = get_user_model()


class Command(BaseCommand):

    def handle(self, *args, **options):
        try:
            print('Creating admin superuser...')
            User.objects.create_superuser(
                username=os.getenv('DJANGO_SUPERUSER_USERNAME', 'user'),
                password=os.getenv('DJANGO_SUPERUSER_PASSWORD', 'password'),
                email=os.getenv('DJANGO_SUPERUSER_EMAIL', 'user@mail.com'),
            )
            print('Superuser created successfully')
        except IntegrityError:
            print('Cannot create superuser, it was already created.')
